var app = angular.module('bindApp',[]);

app.controller('bindCtrl', function ($scope){
  $scope.hitung = function (code){
    var x = +$scope.x;
    var y = +$scope.y;
    switch (code){
      case 1: 
        $scope.jenis = "Penjumlahan";
        $scope.hasil = x + y ;
        break;
      case 2: 
        $scope.jenis = "Pengurangan";
        $scope.hasil = x - y ;
        break;
      case 3: 
        $scope.jenis = "Pembagian";
        $scope.hasil = x / y ;
        break;
      case 4: 
        $scope.jenis = "Perkalian";
        $scope.hasil = x * y ;
        break;
    }
  }
});