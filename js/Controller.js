//module controller
var test = angular.module('TestApp', []);

//controller
test.controller('dataOrang', function($scope) {

  $scope.data = [{nama : 'andi', kota : 'Bandung'},{nama : 'baban', kota : 'Jakarta'},{nama : 'jajang', kota : 'Semarang'}];

  $scope.firstName = "",
  $scope.lastName = "",

  $scope.fullName = function() {
      return $scope.firstName + " " + $scope.lastName;
  }

});

